﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Core.SharedKernel.Entities;

namespace GeekStream.Core.SharedKernel.Abstractions.DataAccess
{
    public interface IArticleRepository
    {
        IEnumerable<Article> GetArticles();
    }
}
