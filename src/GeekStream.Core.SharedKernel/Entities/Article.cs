﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace GeekStream.Core.SharedKernel.Entities
{
    public class Article
    {
       public Article(int id,string title,int userId,string content, string articleDetails,int rating)
        {
            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentException(nameof(title));
            }

            if (string.IsNullOrEmpty(content))
            {
                throw new ArgumentException(nameof(content));
            }

            Id = id;
            Title = title;
            UserId = userId;
            Content = content;
            ArticleDetails = articleDetails;
            Rating = rating;
        }
        public int Id { get; set; }

        public string Title { get; set; }

        public ICollection<Keyword> Keywords { get; set; }
        public string Content { get; set; }
        public DateTime PublishingDate { get; set; }
        public string ArticleDetails { get; set; }
        public int Rating { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
