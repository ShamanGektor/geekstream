﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.SharedKernel.Entities
{
    public class Category
    {
        public Category()
        {

        }

        public int Id { set; get; }

        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<Article> Articles { get; set; }
    }
}
