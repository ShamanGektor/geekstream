﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.SharedKernel.Entities
{
    public class Comment
    {
        public Comment(int id,string content,int userId)
        {
            if (string.IsNullOrEmpty(content))
            {
                throw new ArgumentException(nameof(content));
            }

            Id = id;
            Content = content;
            UserId = userId;
        }

        public int Id { get; set; }

        public string Content { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int ArticleId { get; set; }
        public Article Article { get; set; }

    }
}
