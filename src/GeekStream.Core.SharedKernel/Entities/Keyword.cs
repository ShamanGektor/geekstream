﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.SharedKernel.Entities
{
    public class Keyword
    {
        public Keyword()
        {

        }

        public int Id { get; set; }

        public string Word { get; set; }

        public int ArticleId { get; set; }
        public Article Article { get; set; }
    }
}
