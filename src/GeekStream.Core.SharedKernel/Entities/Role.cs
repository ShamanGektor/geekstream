﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.SharedKernel.Entities
{
    public class Role
    {
        public Role(int id,string name,string permissions)
        {

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException(nameof(name));
            }

            if (string.IsNullOrEmpty(permissions))
            {
                throw new ArgumentException(nameof(permissions));
            }

            Id = id;
            Name = name;
            Permissions = permissions;
        }

        public int Id { get; set; }

        public string Name { get; set; }
        public string Permissions { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
