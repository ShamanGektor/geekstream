﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.SharedKernel.Entities
{
    public class User
    {
        public User(int id,string firstName,string secondName,string email)
        {
            Id = id;
            FirstName = firstName;
            SecondName = secondName;
            Email = email;
        }

        public int Id { get; set; }

        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }

        public ICollection<Article> Articles { get; set; }

        public Role Role { get; set; }
    }
}
