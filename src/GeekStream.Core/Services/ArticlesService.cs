﻿using System.Collections.Generic;
using GeekStream.Core.SharedKernel.Abstractions.DataAccess;
using GeekStream.Core.SharedKernel.Entities;

namespace GeekStream.Core.Services
{
    public class ArticlesService
    {
        private readonly IArticleRepository _articleRepository;

        public ArticlesService(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IEnumerable<Article> GetArticles()
        {
            return _articleRepository.GetArticles();
        }
    }
}
