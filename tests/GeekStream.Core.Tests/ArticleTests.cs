﻿using GeekStream.Core.SharedKernel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GeekStream.Core.Tests
{
    public class ArticleTests
    {
        [Fact]
        public void Constructor_NullTitle_ThrowException()
        {
            string title = null;
            string content = "CONTENT";

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(0, title, 0, content, null, 0);
            });
        }

        [Fact]
        public void Constructor_NullContent_ThrowException()
        {
            string title = "TITLE";
            string content = null;

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(0, title, 0, content, null, 0);
            });
        }


        [Fact]
        public void Constructor_AllRight_Successfully()
        {
            string title = "TITLE";
            string content = "CONTENT";

            var result = new Article(0, title, 0,  content, null, 0);

            Assert.NotNull(result);

        }

    }
}