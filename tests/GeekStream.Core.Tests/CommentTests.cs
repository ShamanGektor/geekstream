﻿using GeekStream.Core.SharedKernel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GeekStream.Core.Tests
{
    public class CommentTests
    {
        [Fact]
        public void Constructor_NullContent_ThrowException()
        {
            int id = 0;
            string content = null;
            int authorId = 0;

            Assert.Throws<ArgumentException>(() =>
            {
                var comment = new Comment(id,content, authorId);
            });
        }

        [Fact]
        public void Constructor_AllRight_Successfully()
        {
            int id = 0;
            string content = "Some content";
            int authorId = 0;

            var result = new Comment(id, content, authorId);

            Assert.NotNull(result);
        }
    }
}
