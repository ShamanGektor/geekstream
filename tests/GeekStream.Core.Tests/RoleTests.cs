﻿using GeekStream.Core.SharedKernel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GeekStream.Core.Tests
{
    public class RoleTests
    {
        [Fact]
        public void Constructor_NullName_ThrowException()
        {
            int id = 0;
            string name = null;
            string permissions = "read";

            Assert.Throws<ArgumentException>(() =>
            {
                var role = new Role(id, name, permissions);
            });
        }

        [Fact]
        public void Constructor_NullPermissions_ThrowException()
        {
            int id = 0;
            string name = "author";
            string permissions = null;

            Assert.Throws<ArgumentException>(() =>
            {
                var role = new Role(id, name, permissions);
            });
        }

        [Fact]
        public void Constructor_AllRight_Successfully()
        {
            int id = 0;
            string name = "author";
            string permissions = "read";

            var result = new Role(id, name, permissions);

            Assert.NotNull(result);
        }
    }
}
